=============
BitbucketSync
=============

BitbucketSync is a python command line utility that lets you synchronize all repositories that you own (default) on Bitbucket to a local directory on your machine. By using the `--with-aliens` switch, you can also synchronize repositories that other developers shared with you. The directory names of the ones that got shared end with two underscores and the developer's name (so forks with the same name don't screw things up).


Installation
------------
Just grab a copy of sync.py. There are no dependencies beside the `hg` command, anything else is just the Python Standard Library.


Command Line Usage
------------------
    Usage: sync.py [options]

    Options:
      -h, --help            show this help message and exit
      -u USERNAME, --username=USERNAME
                            Your username on bitbucket.org 
      -p PASSWORD, --password=PASSWORD
                            Your password on bitbucket.org
      -d DIRECTORY, --directory=DIRECTORY
                            The local directory to sync to [Defaults to the
                            current directory]
      -a, --with-aliens     Also sync repositories that other people shared with
                            you
      -v, --verbose         Verbose output


Speed
-----
To speed things up, all repositories are fetched in parallel. Make sure that you enabled SSH compression for bitbucket.org by following the steps on `Enabling Compression`_.


.. _Enabling Compression: http://confluence.atlassian.com/display/BITBUCKET/Using+SSH+to+Access+your+Bitbucket+Repository#UsingSSHtoAccessyourBitbucketRepository-EnablingCompression
