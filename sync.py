#!/usr/bin/env python
import getpass
try:
    import json
except ImportError:
    import simplejson as json
import optparse
import os
import subprocess
import sys
import urllib2


def _get_repositories(username, password):
    auth_value = ('%s:%s' % (username, password)).encode('base64').strip()
    headers = {'Authorization': 'Basic %s' % auth_value}
    url = 'https://bitbucket.org/api/2.0/repositories/%s' % username
    values = []

    while url is not None:
        request = urllib2.Request(url, None, headers)
        data = json.loads(urllib2.urlopen(request).read())
        values = values + data['values']
        url = data.get('next')

    return values


def _hg_clone(directory, sub_dir_name, owner, slug, verbose=False):
    os.chdir(directory)
    cmd = 'hg clone ssh://hg@bitbucket.org/%s/%s %s' % (owner, slug,
                                                        sub_dir_name)
    proc = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
    ret_value = proc.wait()
    msg = proc.stdout.read()
    sys.stdout.write('%s%s%s%s' % (sub_dir_name, os.linesep,
                                   '=' * len(sub_dir_name), os.linesep))
    sys.stdout.write("%s%s" % (msg, os.linesep))
    return ret_value


def _hg_pull(directory, sub_dir_name, owner, slug, verbose=False):
    working_dir = os.path.join(directory, sub_dir_name)
    os.chdir(working_dir)
    cmd = 'hg pull'
    proc = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
    ret_value = proc.wait()
    msg = proc.stdout.read()
    if verbose or "no changes found" not in msg:
        sys.stdout.write('%s%s%s%s' % (sub_dir_name, os.linesep,
                                       '=' * len(sub_dir_name), os.linesep))
        sys.stdout.write("%s%s" % (msg, os.linesep))
    return ret_value


if __name__ == '__main__':
    parser = optparse.OptionParser()
    parser.add_option('-u', '--username', action='store',
                      help='Your username on bitbucket.org ')
    parser.add_option('-p', '--password', action='store',
                      help='Your password on bitbucket.org')
    parser.add_option('-d', '--directory', action='store', default=os.getcwd(),
                      help='The local directory to sync to [Defaults to '
                      'the current directory]')
    parser.add_option('-a', '--with-aliens', action='store_true',
                      dest='aliens', help='Also sync repositories that '
                      'other people shared with you')
    parser.add_option('-v', '--verbose', action='store_true', dest='verbose',
                      help='Verbose output')
    (options, args) = parser.parse_args()

    username = options.username or raw_input('Your bitbucket username: ')
    password = options.password or getpass.getpass('Your bitbucket password: ')
    if not os.path.isdir(options.directory):
        parser.error('Directory is required and must exist')

    sub_dirs = os.walk(options.directory).next()[1]

    for repo_data in _get_repositories(username, password):
        (owner, slug) = repo_data['full_name'].split('/')

        if owner == username:
            sub_dir_name = slug
        elif options.aliens:
            sub_dir_name = '%s__%s' % (slug, owner)
        else:
            continue

        fn_route = _hg_clone if sub_dir_name not in sub_dirs else _hg_pull
        fn_route(options.directory, sub_dir_name, owner, slug, options.verbose)
